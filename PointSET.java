import java.util.Comparator;
import java.util.Collections;
import java.util.TreeSet;
import java.util.Iterator;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.Queue;

public class PointSET {

    private TreeSet<Point2D> pointSet;
    private Iterator<Point2D> pointsIterator;

    // construct an empty set of points
    public PointSET(){
        pointSet = new TreeSet<Point2D>();
    }

    // is the set empty?
    public boolean isEmpty(){
        return pointSet.isEmpty();
    }

    // number of points in the set
    public int size(){
        return pointSet.size();
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p){
        if (p == null) throw new java.lang.IllegalArgumentException();
        pointSet.add(p);
    }

    // does the set contain point p?
    public boolean contains(Point2D p){
        if (p == null) throw new java.lang.IllegalArgumentException();
        return pointSet.contains(p);
    }

    // draw all points to standard draw
    public void draw(){
        pointsIterator = pointSet.iterator();
        Point2D p;
        while(pointsIterator.hasNext()){
            p = (Point2D) pointsIterator.next();
            p.draw();
        }
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect){
        if (rect == null) throw new java.lang.IllegalArgumentException();
        Queue<Point2D> pointsInRange = new Queue<>();
        Point2D p;

        pointsIterator = pointSet.iterator();
        while(pointsIterator.hasNext()){
            p = (Point2D) pointsIterator.next();
            if (rect.contains(p))
                pointsInRange.enqueue(p);
        }
        return pointsInRange;
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p){
        if (p == null) throw new java.lang.IllegalArgumentException();
        Point2D closestPoint = null, next = null;
        double shortestDistance = Double.POSITIVE_INFINITY;
        pointsIterator = pointSet.iterator();
        while(pointsIterator.hasNext()){
            next = (Point2D) pointsIterator.next();
            if (p.distanceTo(next) < shortestDistance) {
                closestPoint = next;
                shortestDistance = p.distanceTo(closestPoint);
            }
        }
        return closestPoint;
    }

    // unit testing of the methods (optional)
    public static void main(String[] args){

    }
}