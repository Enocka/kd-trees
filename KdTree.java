import java.util.Iterator;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;


public class KdTree {
    private Node root;
    private static final boolean VERTICAL = true;
    private static final boolean HORIZONTAL = false;
    private Queue<Point2D> pointsInRange = new Queue<>();
    private Point2D nearestPoint;
    private double shortestDistance = Double.POSITIVE_INFINITY;

    private class Node implements Comparable<Node>{
        private Point2D point2D;
        private Node left, right, parent;
        private int count;

        private boolean verticalAlignment;

        public Node(Point2D p, boolean alignment, Node parent){
            this.point2D = p;
            this.verticalAlignment = alignment;
            this.left = null;
            this.right = null;
            this.parent = parent;
            this.count = 1;
        }

        public boolean isVerticalAlignment(){
            return verticalAlignment;
        }

        //other option: xCompare, yCompare
        public int compareTo(Node that){
            if(verticalAlignment){
                if (this.point2D.x() < that.point2D.x())
                    return -1;
                else if (this.point2D.x() > that.point2D.x())
                    return 1;
                else if (this.point2D.y() < that.point2D.y())
                    return -1;
                else if (this.point2D.y() > that.point2D.y())
                    return 1;
                else return 0;
            }
            else if (this.point2D.y() < that.point2D.y())
                return -1;
            else if (this.point2D.y() > that.point2D.y())
                return 1;
            else if (this.point2D.x() < that.point2D.x())
                return -1;
            else if (this.point2D.x() > that.point2D.x())
                return 1;
            else return 0;
        }
    }

    public KdTree(){
//        nearestPoint = null;
    }

    // is the set empty?
    public boolean isEmpty(){
        return root == null;
    }

    // number of points in the set
    public int size(){
        return size(root);
    }

    private int size(Node x){
        if (x == null) return 0;
        return x.count;
    }

    // add the point to the set (if it is not already in the set)
    public void insert(Point2D p){
        root = insert(root, p, HORIZONTAL);
    }

    private Node insert(Node x, Point2D p, boolean parentAlignment){
        Node newNode = new Node(p, !parentAlignment, x);
        if (x == null) return newNode;
        int cmp = newNode.compareTo(x);
        if (cmp < 0)
            x.left = insert(x.left, p, x.verticalAlignment);
        else if (cmp > 0)
            x.right = insert(x.right, p, x.verticalAlignment);
        else
            x.point2D = p;
        x.count = 1 + size(x.left) + size(x.right);
        return x;
    }

    // does the set contain point p?
    public boolean contains(Point2D p){
        Point2D found = get(p);
        if (found == null) return false;
        else return true;
    }

    private Point2D get(Point2D p){
        Node x = root;
        Node current;
        while(x != null){
            current = new Node(p, !x.verticalAlignment, x);
            int cmp = x.compareTo(current);
            if      (cmp < 0)
                x = x.right;
            else if (cmp > 0)
                x = x.left;
            else     return x.point2D;
        }
        return null;
    }

    // draw all points to standard draw
    public void draw(){
        pointsInRange.forEach(point2D -> point2D.draw());
    }

    // all points that are inside the rectangle (or on the boundary)
    public Iterable<Point2D> range(RectHV rect){
        checkInRange(root, rect);
        return pointsInRange;
    }

    private void checkInRange(Node x, RectHV rect){
        if (x == null) return;

        if(rect.contains(x.point2D)){
            pointsInRange.enqueue(x.point2D);
        }
        if (x.verticalAlignment) {
            if(x.point2D.x() > rect.xmax()){
                checkInRange(x.left, rect);
            }
            else if (x.point2D.x() < rect.xmin()){
                checkInRange(x.right, rect);
            }
            else {
                checkInRange(x.left, rect);
                checkInRange(x.right, rect);
            }
        } else {
            if(x.point2D.y() > rect.ymax()){
                checkInRange(x.left, rect);
            }
            else if (x.point2D.y() < rect.ymin()){
                checkInRange(x.right, rect);
            }
            else {
                checkInRange(x.left, rect);
                checkInRange(x.right, rect);
            }
        }
    }

    // a nearest neighbor in the set to point p; null if the set is empty
    public Point2D nearest(Point2D p){
        getNearest(root, p);
        return nearestPoint;
    }

    private void getNearest(Node n, Point2D p){
        if(n == null) return;
        if (n.point2D.distanceTo(p) < shortestDistance) {
            nearestPoint = n.point2D;
            shortestDistance = n.point2D.distanceTo(p);
        }
        if(n.verticalAlignment){
            if (n.point2D.x() > p.x()){
                getNearest(n.left, p);
                if (n.parent != null && n.parent.point2D.distanceTo(p) < n.point2D.distanceTo(p));
                else
                    getNearest(n.right, p);
            }
            else {
                getNearest(n.right, p);
                if (n.parent != null && n.parent.point2D.distanceTo(p) < n.point2D.distanceTo(p));
                else
                    getNearest(n.left, p);
            }
        } else {
            if (n.point2D.y() > p.y()){
                getNearest(n.left, p);
                if (n.parent != null && n.parent.point2D.distanceTo(p) < n.point2D.distanceTo(p));
                else
                    getNearest(n.right, p);
            }
            else {
                getNearest(n.right, p);
                if (n.parent != null && n.parent.point2D.distanceTo(p) < n.point2D.distanceTo(p));
                else
                    getNearest(n.left, p);
            }
        }
        return;
    }

    public static void main(String[] args){
        Point2D p1 = new Point2D(0.7, 0.2);
        Point2D p2 = new Point2D(0.5, 0.4);
        Point2D p3 = new Point2D(0.2, 0.3);
        Point2D p4 = new Point2D(0.4, 0.7);
        Point2D p5 = new Point2D(0.9, 0.6);

        Point2D queryPoint = new Point2D(0.817, 0.333);
        KdTree tree = new KdTree();
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        Point2D nearestPoint = tree.nearest(queryPoint);
        System.out.println("student nearest point is = "+ nearestPoint.x() + ", "+ nearestPoint.y());
        tree.insert(p1);
        System.out.println("after adding p1, "+ p1.x() + ", " + p1.y() +" whose distance from the point is  :"+ p1.distanceTo(queryPoint));
        tree.nearest(queryPoint);
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        tree.insert(p2);
        System.out.println("after adding p2, "+ p2.x() + ", " + p2.y() +" whose distance from the point is  :"+ p2.distanceTo(queryPoint));
        tree.nearest(queryPoint);
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        tree.insert(p3);
        System.out.println("after adding p3, "+ p3.x() + ", " + p3.y() +" whose distance from the point is  :"+ p3.distanceTo(queryPoint));
        tree.nearest(queryPoint);
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        tree.insert(p4);
        System.out.println("after adding p4, "+ p4.x() + ", " + p4.y() +" whose distance from the point is  :"+ p4.distanceTo(queryPoint));
        tree.nearest(queryPoint);
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        tree.insert(p5);
        System.out.println("after adding p5, "+ p5.x() + ", " + p5.y() +" whose distance from the point is  :"+ p5.distanceTo(queryPoint));
        System.out.println("the shortest distance is :"+ tree.shortestDistance);
        nearestPoint = tree.nearest(queryPoint);
        System.out.println("student nearest point is = "+ nearestPoint.x() + ", "+ nearestPoint.y());

    }
}
